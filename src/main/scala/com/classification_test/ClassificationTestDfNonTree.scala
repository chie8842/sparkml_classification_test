package com.classification_test

//import org.apache.spark.{SparkConf, SparkContext}
//import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.{DataFrame, SparkSession}

import org.apache.spark.ml.{Pipeline, PipelineModel, Transformer}
import org.apache.spark.ml.classification._
import org.apache.spark.ml.evaluation._
import org.apache.spark.ml.feature._
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.util.MLUtils

import java.text.SimpleDateFormat
import org.apache.commons.lang3.time.StopWatch

object ClassificationTestDfNonTree {
    
    def main(args: Array[String]){

        //data = {a9a_100,a9a_1000,a9a_20000,
        //        rcv1.binary_15000,rcv1.multiclass_15000,
        //        cod-rna_2000,rcv1.binary_20000}
        
        //modelType = {dt,rf,gbt,lr,nb}
        val dataName = args(0)
        val dataNum = args(3)
        val modelType = args(1)
        val prefix = args(2)
        val inputPath = "s3://sampledata-emr/input"
        val trainPath = s"$inputPath/train"
        val testPath = s"$inputPath/test"
        val modelPath = s"s3://sampledata-emr/output/model/$modelType-$prefix"
        val resultPath = s"s3://sampledata-emr/output/result/$dataName${dataNum}-$modelType"

        //val conf = new SparkConf().setAppName(s"$dataName${dataNum}-$modelType")
        //val sc = new SparkContext(conf)
        //val sqlContext = new SQLContext(sc)
        val sqlContext = SparkSession.builder.appName(s"$dataName${dataNum}-$modelType").getOrCreate()

        val trainData = sqlContext.read.format("libsvm").load(s"$trainPath/${dataName}$dataNum")
        val testData = sqlContext.read.format("libsvm").load(s"$testPath/$dataName.t_3000")
 
        def gen_model(): (PipelineModel, String) = {
            val labelIndexer = new StringIndexer().
                setInputCol("label").
                setOutputCol("indexedLabel").
                fit(trainData)
        
            val featureIndexer = new VectorIndexer().
                setInputCol("features").
                setOutputCol("indexedFeatures").
                fit(trainData)
        
            val learn = modelType match{
                case "gbt" => 
                    new GBTClassifier().
                    setLabelCol("indexedLabel").
                    setFeaturesCol("indexedFeatures")
        
                case "lr" => 
                    new LogisticRegression().
                    setLabelCol("indexedLabel").
                    setFeaturesCol("indexedFeatures")
        
                case "nb" => 
                    new NaiveBayes().
                    setLabelCol("indexedLabel").
                    setFeaturesCol("indexedFeatures")
        
        }
            val learnParams = learn.extractParamMap.toString()
        
            val labelConverter = new IndexToString().
                setInputCol("prediction").
                setOutputCol("predictedLabel").
                setLabels(labelIndexer.labels)
        
            val pipeline = new Pipeline().
                setStages(Array(labelIndexer, featureIndexer, learn, labelConverter))
        
            val model = pipeline.fit(trainData)
            //model.write.overwrite().save(modelPath)

            (model, learnParams)
        }
        
        def predict(model: PipelineModel){
 
            val clmodel = modelType match{
                case "lr" => 
                    model.stages(2).asInstanceOf[LogisticRegressionModel]
        
                case "nb" => 
                    model.stages(2).asInstanceOf[NaiveBayesModel]
            }

            val fullPredictions = clmodel.setFeaturesCol("features").transform(testData)
            val predictions = fullPredictions.select("prediction").rdd.map(_.getDouble(0))
            val labels = fullPredictions.select("label").rdd.map(_.getDouble(0))

            fullPredictions.show(5,false)
            fullPredictions.write.mode("overwrite").parquet(resultPath)
            val accuracy = new MulticlassMetrics(predictions.zip(labels)).accuracy
            println(s"Accuracy : $accuracy")

            fullPredictions.show(5,false)
            fullPredictions.write.mode("overwrite").parquet(resultPath)
        
//            println("modelType="+ modelType + ", param = " + clmodel.params.mkString(","))
       
        }
     

        val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        val genStartTime = dateFormat.format(new java.util.Date())
    
        val genStopWatch = new StopWatch()
        genStopWatch.start()
        val (model, learnParams) = gen_model()
        genStopWatch.split()
        
        val genProcessTime = genStopWatch.toSplitString
        val genEndTime = dateFormat.format(new java.util.Date())

        genStopWatch.stop()

        if (dataNum.isEmpty) {
            val predStartTime = dateFormat.format(new java.util.Date())
    
            val predStopWatch = new StopWatch()
            predStopWatch.start()
            predict(model)
            predStopWatch.split()
            
            val predProcessTime = predStopWatch.toSplitString
            val predEndTime = dateFormat.format(new java.util.Date())

            predStopWatch.stop()

            println(s"predict: [start time] $predStartTime")
            println(s"predict: [process time] $predProcessTime")
            println(s"predict: [end time] ${predEndTime}")

        }

        println(s"modelType = $modelType")
        println(s"params = $learnParams")

        println(s"gen_model: [start time] $genStartTime")
        println(s"gen_model: [process time] ${genProcessTime}")
        println(s"gen_model: [end time] ${genEndTime}")

    }
}
