#/bin/bash

spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar a9a rf 1 "" > log/a9a_rf.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.binary rf 1 "" > log/rcv1.binary_rf.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.multiclass rf 1 "" > log/rcv1.multiclass_rf.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar cod-rna rf 1 "" > log/cod-rna_rf.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar a9a gbt 1 "" > log/a9a_gbt.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.binary gbt 1 "" > log/rcv1.binary_gbt.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.multiclass gbt 1 "" > log/rcv1.multiclass_gbt.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar cod-rna gbt 1 "" > log/cod-rna_gbt.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar a9a lr 1 "" > log/a9a_lr.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.binary lr 1 "" > log/rcv1.binary_lr.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.multiclass lr 1 "" > log/rcv1.multiclass_lr.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar cod-rna lr 1 "" > log/cod-rna_lr.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar a9a nb 1 "" > log/a9a_nb.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.binary nb 1 "" > log/rcv1.binary_nb.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar rcv1.multiclass nb 1 "" > log/rcv1.multiclass_nb.log 1>&2
spark-submit --master yarn --deploy-mode client --num-executors 5 --class com.classification_test.ClassificationTestDf target/scala-2.10/classification_test-assembly-1.0.jar cod-rna nb 1 "" > log/cod-rna_nb.log 1>&2
