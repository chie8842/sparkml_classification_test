# -*- coding:utf-8 -*-

import os
import sys
import commands

org_cmd = "time spark-submit --master yarn --deploy-mode client --executor-cores 2 --executor-memory  5g --num-executors 4 --class com.classification_test.ClassificationTestDf target/scala-2.11/classification_test-assembly-1.0.jar"
org_cmd_tree = "time spark-submit --master yarn --deploy-mode client --executor-cores 2 --executor-memory  5g --num-executors 4 --class com.classification_test.ClassificationTestDfTree target/scala-2.11/classification_test-assembly-1.0.jar"
org_cmd_nontree = "time spark-submit --master yarn --deploy-mode client --executor-cores 2 --executor-memory  5g --num-executors 4 --class com.classification_test.ClassificationTestDfNonTree target/scala-2.11/classification_test-assembly-1.0.jar"

file = "commands_onlytree.txt"

if os.path.exists(file):
    os.remove(file)
f = open(file,'a')

for mtype in ["dt","rf","gbt"]:
    for data in [("a9a", "_100"),("a9a", "_1000"),("a9a", "_20000"),("rcv1.binary", "_15000"),("rcv1.multiclass", "_15000"),("cod-rna", "_20000"),("rcv1.binary", "_20000")]:
        cmd = org_cmd_tree + " " + data[0] + " " + mtype + " 1 " + str(data[1]) + " > log/" + data[0] + str(data[1]) + "_" + mtype + ".log 1>&2"
        print(cmd)
        f.write(cmd + "\n")
        #os.system(org_cmd)

#for mtype in ["dt","rf","gbt"]:
#    for data in [("a9a","\"\""),("rcv1.binary","\"\""),("rcv1.multiclass","\"\""),("cod-rna","\"\"")]:
#        cmd = org_cmd_tree + " " + data[0] + " " + mtype + " 1 " + str(data[1]) + " > log/" + data[0] + "_" + mtype + ".log 1>&2"
#        print(cmd)
#        f.write(cmd + "\n")

f.close

